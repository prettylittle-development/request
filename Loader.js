
// https://github.com/jacoborus/nanobar/blob/master/nanobar.js

class Loader
{
	constructor(config = {})
	{
		let defaults	=
		{
			contentType			: 'application/x-www-form-urlencoded',
			cachebust			: false,
			show_progress		: false
		};

		this.config				= Object.assign(defaults, config);

		return this;
	}

	parse(req)
	{
		var result;

		try
		{
			result 				= JSON.parse(req.responseText);
		}
		catch (e)
		{
			result 				= req.responseText;
		}

		return [result, req];
	}

	xhr(type, url, data)
	{
		if(!url)
		{
			throw "Url is not defined";
		}

		if(this.config.cachebust)
		{
			url				   += (url.indexOf('?') > -1) ? '' : '?s=' + Date.now();
		}

		let methods	=
		{
			success	: function () {},
			error	: function () {},
			always	: function () {}
		};

		let XHR					= XMLHttpRequest || ActiveXObject;
		let request				= new XHR('MSXML2.XMLHTTP.3.0');
		let self				= this;

		request.open(type, url, true);

		request.setRequestHeader('Content-type', this.config.contentType);
		request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest');

		if(self.config.show_progress)
		{
			console.log("SHOW PROGRESS");

			//var bar				= dom.create('<div class="page-loader"></div>');
			//	bar				= bar.firstChild;

			//document.body.appendChild(bar);
		}

		request.onreadystatechange = function ()
		{
			var req;

			if (request.readyState === 4)
			{
				req			= self.parse(request);

				if (request.status >= 200 && request.status < 300)
				{
					methods.success.apply(methods, req);
				}
				else
				{
					methods.error.apply(methods, req);
				}

				if(self.config.show_progress)
				{
					//console.log("KILL PROGRESS");
				}

				methods.always.apply(methods, req);
			}
		};

		// do the request
		request.send(data);

		var obj =
		{
			success: function (callback)
			{
				methods.success	= callback;

				return obj;
			},
			error: function (callback)
			{
				methods.error 	= callback;

				return obj;
			},
			always: function (callback)
			{
				methods.always	= callback;

				return obj;
			}
		};

		return obj;
	}

	get(src)
	{
		return this.xhr('GET', src);
	}

	put(url, data)
	{
		return this.xhr('PUT', url, data);
	}

	post(url, data)
	{
		return this.xhr('POST', url, data);
	}

	delete(url)
	{
		return this.xhr('DELETE', url);
	}

	// TODO - set content type method
}

export default Loader;
